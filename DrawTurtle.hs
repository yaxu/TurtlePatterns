{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}
module DrawTurtle where
import Prelude hiding (Left, Right)
import qualified Graphics.WorldTurtle as WT
import qualified Graphics.Gloss.Data.Picture as G
import           Control.Monad
import           IntermediateNotation 
import           GetTidalTime
import qualified Sound.Tidal.Context as Tidal
import           ParseTurtle
import qualified Data.Map as M
import qualified Data.List as L

turtle2Logo :: Turtle -> WT.TurtleCommand ()
turtle2Logo turtle = case turtle of
        Forward a  -> drawLine a
        Left a     -> turnLeft a
        Right a    -> turnLeft (-a)

drawLine :: Float -> WT.TurtleCommand ()
drawLine a = WT.forward $ moveForwardBy $ a

turnLeft :: Float -> WT.TurtleCommand ()
turnLeft a = WT.left $ turnLeftBy $ a

moveForwardBy :: Floating a => a -> a
moveForwardBy a = a * 10.0

turnLeftBy :: Floating a => a -> a
turnLeftBy a = a * 180.00 / pi

--shape2Logo :: Shape -> WT.TurtleCommand ()
shape2Logo :: Monad m => Shape -> m (WT.TurtleCommand ())
shape2Logo shape = case shape of
        Square a    -> drawShape [Forward a, Right (pi / 2)] 4
        Triangle a  -> drawShape [Forward a, Left (2 * pi /3)] 3
        Circle a    -> drawCircle a 

--drawSquare :: Float -> WT.TurtleCommand ()
drawShape :: Monad m => [Turtle] -> Int -> m (WT.TurtleCommand ())
drawShape turtles rep = do
       result <- combineTurtles turtles
       return $ WT.repeatFor rep result

       --turtle2Logo (Forward 10) >> turtle2Logo (Right (pi / 2))

combineTurtles :: Monad m => [Turtle] -> m (WT.TurtleCommand [()])
combineTurtles turtles = do
        return $ sequenceA $ mapTurtles turtles

mapTurtles :: [Turtle] -> [WT.TurtleCommand ()]
mapTurtles turtles = map turtle2Logo turtles

mapTurtlesByFactor :: [Turtle] -> Float -> [WT.TurtleCommand ()]
mapTurtlesByFactor turtles factor = mapTurtles $ multiplyTurtlesByFactor factor turtles

mapTurtles' :: Show a => a -> IO (WT.TurtleCommand ())
mapTurtles' turtles =  do
        print turtles
        return $ turtle2Logo (Forward 10) >> turtle2Logo (Right (pi / 2))

drawTriangle :: Float -> Int -> WT.TurtleCommand ()
drawTriangle size rep = WT.repeatFor rep $ 
        turtle2Logo (Forward size) >> turtle2Logo (Right (2 * pi /3)) 

--drawCircle :: Float -> WT.TurtleCommand ()
drawCircle :: Monad m => Float -> m (WT.TurtleCommand ())
drawCircle size = do
        return $ WT.circle size


shape2Turtle :: Shape -> [Turtle]
shape2Turtle shape = case shape of
        Square a -> [Forward a, Right (pi / 2)]

fib :: (Eq t, Num a, Num t) => t -> [a]
fib 0 = [0]
fib 1 = [0, 1]
fib n = fib (n - 1) ++ [sum [last prev, last (init prev)]] 
    where prev = fib $ n - 1

spiral :: MetaShape
spiral = Spiral { shape = Square 2, shifts = [Forward 10, Right (pi/2)], delta = 0.5 }

metashape2Logo :: Monad m => MetaShape -> Float -> m (WT.TurtleCommand ())
metashape2Logo metashape = case metashape of 
        Spiral shape shift delta   -> drawSpiral shape shift delta

metashape2Logo' :: Monad m => MetaShape -> m [WT.TurtleCommand ()]
metashape2Logo' metashape = case metashape of 
        Spiral shape shift delta   -> drawSpiral' shape shift delta 1000

drawSpiral :: Monad m => Shape -> a -> b -> Float -> m (WT.TurtleCommand ())
drawSpiral shape shift increment = do
        result <- drawSingleShapeOfSpiral shape shift 
        return result

--drawSpiral' ::  Shape -> t -> Float -> Int -> [WT.TurtleCommand ()]
drawSpiral' :: (Monad m, Eq t, Num t) =>
        Shape -> p -> Float -> t -> m [WT.TurtleCommand ()]
drawSpiral' shape shift delta rep = do
        forM (fib rep) $ \f -> do
           result  <- drawSingleShapeOfSpiral (addDelta2Shape shape (delta*f)) shift f
           return result

addDelta2Shape :: Shape -> Float -> Shape
addDelta2Shape (Square a) delta = Square (a + delta)
addDelta2Shape (Triangle a) delta = Triangle (a + delta)
addDelta2Shape (Circle a) delta = Circle (a + delta)

drawSingleShapeOfSpiral :: Monad m => Shape -> p -> Float -> m (WT.TurtleCommand ())
drawSingleShapeOfSpiral shape shift factor = do
        result <- shape2TurtlesSeq shape shift factor
        return $ sequence_ result

shape2TurtlesSeq :: Monad m => Shape -> p -> Float -> m [WT.TurtleCommand ()]
shape2TurtlesSeq shape shift factor = do
        result <- shape2Logo shape
        return [result, sequenceTurtles' (shifts spiral) factor]

sequenceTurtles' :: [Turtle] -> Float -> WT.TurtleCommand ()
sequenceTurtles' turtles factor = sequence_ $ mapTurtlesByFactor turtles factor

sequenceTurtles :: [Turtle] -> WT.TurtleCommand ()
sequenceTurtles turtles = sequence_ $ mapTurtlesByFactor turtles 10

intersperseSequence :: [Turtle] -> WT.TurtleCommand () -> WT.TurtleCommand ()
intersperseSequence turtles command = sequence_ $ intersperseCommand  
        (mapTurtlesByFactor turtles 10) command

intersperseCommand :: [WT.TurtleCommand ()] -> WT.TurtleCommand () -> [WT.TurtleCommand ()]
intersperseCommand turtleCommands command = L.intersperse command turtleCommands

multiplyTurtlesByFactor :: Float -> [Turtle] -> [Turtle]
multiplyTurtlesByFactor factor turtles = map (\t -> multiplyT factor t) turtles

multiplyT :: Float -> Turtle -> Turtle
multiplyT factor turtle = case turtle of 
       Forward a -> Forward (a * factor)
       Right   a -> Right   (a * factor)
       Left    a -> Left    (a * factor)

multiplyT' :: Float -> Float -> Turtle -> Turtle
multiplyT' lengthFactor angleFactor turtle = case turtle of 
       Forward a -> Forward (a * lengthFactor)
       Right   a -> Right   (a * angleFactor)
       Left    a -> Left    (a * angleFactor)

multiplyTByFactors :: Float -> Float -> [Turtle] -> [Turtle]
multiplyTByFactors lengthFactor angleFactor turtles = 
       map (\t -> multiplyT' lengthFactor angleFactor t) turtles
