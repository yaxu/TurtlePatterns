{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_TurtlePatterns (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/saachi/.cabal/bin"
libdir     = "/home/saachi/.cabal/lib/x86_64-linux-ghc-8.8.4/TurtlePatterns-0.1.0.0-6bNFrJigNLQ3iTk5m86lhv"
dynlibdir  = "/home/saachi/.cabal/lib/x86_64-linux-ghc-8.8.4"
datadir    = "/home/saachi/.cabal/share/x86_64-linux-ghc-8.8.4/TurtlePatterns-0.1.0.0"
libexecdir = "/home/saachi/.cabal/libexec/x86_64-linux-ghc-8.8.4/TurtlePatterns-0.1.0.0"
sysconfdir = "/home/saachi/.cabal/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "TurtlePatterns_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "TurtlePatterns_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "TurtlePatterns_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "TurtlePatterns_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "TurtlePatterns_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "TurtlePatterns_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
