{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}
module DrawPattern where
import           DrawTurtle
import Prelude hiding (Left, Right)
import qualified Graphics.WorldTurtle as WT
import qualified Graphics.Gloss.Data.Picture as G
import qualified Sound.Tidal.Context as Tidal
import           IntermediateNotation
import           GetTidalTime
import           ParseTurtle

multiplyByTime :: Tidal.Time -> Turtle -> Turtle
multiplyByTime time turtle = multiplyT (fromRational time) turtle

multiplyEventsMap :: [(Tidal.Time, Turtle)] -> [Turtle]
multiplyEventsMap = map (\(f, t) -> multiplyByTime f t) 

pattern2Turtles :: Tidal.Pattern Turtle -> [Turtle]
pattern2Turtles pat = multiplyEventsMap eventsMap
       where 
           eventsMap = zip (eventTimes evs) turtles
           turtles = multiplyTByFactors 10 (pi / 2) $ eventsValues evs
           evs = sortEventsByArc pat (makeArc 0 400)

resultpat :: [Turtle]
resultpat = pattern2Turtles $ Tidal.slowSqueeze "1 3 1" ("[l f, f r]" :: Tidal.Pattern Turtle)
--resultpat = pattern2Turtles ("[l, r]" :: Tidal.Pattern Turtle)

{-
main :: IO ()
main = do
       --let colorShift = WT.penColor >>= WT.setPenColor . WT.shiftHue 1
       let colorShift = WT.penColor >>= WT.setPenColor . WT.shiftHue 1
       let resultpat' = pattern2Turtles $ Tidal.slow "1 1 2 3 5 8" tripat
       WT.runTurtle' WT.black $ do
           WT.setPenColor WT.red
           (intersperseSequence (resultpat) colorShift)
-}
