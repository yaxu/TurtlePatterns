{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, OverloadedStrings #-}
-- :set -XTypeSynonymInstances -XFlexibleInstances
module ParseTurtle where 

import           Text.Parsec.Error
import           Text.ParserCombinators.Parsec
import           Text.ParserCombinators.Parsec.Language ( haskellDef )
import qualified Text.ParserCombinators.Parsec.Token as P
import qualified Text.Parsec.Prim
import qualified IntermediateNotation as IN
import           Sound.Tidal.ParseBP
import           Sound.Tidal.Pattern
import           Sound.Tidal.Context
import           Prelude hiding (Left, Right)


pRight :: MyParser (TPat IN.Turtle)
pRight = wrapPos $ do char 'r' <?> "r (Right)"
                      return (TPat_Atom Nothing $ IN.Right 1)

pLeft :: MyParser (TPat IN.Turtle)
pLeft = wrapPos $ do char 'l' <?> "l (Left)"
                     return (TPat_Atom Nothing $ IN.Left 1)

pForward :: MyParser (TPat IN.Turtle)
pForward = wrapPos $ do char 'f' <?> "f (Forward)"
                        return (TPat_Atom Nothing $ IN.Forward 1)

pTurtle :: MyParser (TPat IN.Turtle)
pTurtle = pRight <|> pLeft <|> pForward

instance Parseable IN.Turtle where
  tPatParser = pTurtle
  doEuclid = euclidOff
  getControl = error "not defined"

instance Enumerable IN.Turtle where
  fromTo a b = fastFromList [a,b]
  fromThenTo a b c = fastFromList [a,b,c]

--global const
starpat = "l f l f" :: Pattern IN.Turtle --makes a star
--factor is pi / 2
squarepat = "f l" :: Pattern IN.Turtle -- makes a square (factor is pi / 2)
tripat = "f l l" :: Pattern IN.Turtle -- makes a triangle 
hexapat2 = "f f l" :: Pattern IN.Turtle 
spiralmesh = "f l l f r r f l l" :: Pattern IN.Turtle
squaremesh = "f <l r> f <r l r>" :: Pattern IN.Turtle
squareLogo = "f [<l r> f ] <r l r> f" :: Pattern IN.Turtle
ninjaLogo = "f [f <l r> f] [f <r l r> f] <l r> f" :: Pattern IN.Turtle
pat = "f l l [f r r f l l f r r] f l l" :: Pattern IN.Turtle

--factor is pi / 3
nonagon = "f l l" :: Pattern IN.Turtle -- makes a nonagon when the the factor is pi /3
hexagon = "f l" :: Pattern IN.Turtle --makes a hexagon when the factor is pi / 3
steps = "f <l r>" :: Pattern IN.Turtle -- makes a step like sequence
hexapat = "f*100 l" :: Pattern IN.Turtle --makes no difference, still makes it the same size
ninjaStar = "f [l f] l" :: Pattern IN.Turtle 
curvedNinjaStar = "f [l f l f] l" :: Pattern IN.Turtle
spiralpat = "f f l" :: Pattern IN.Turtle -- spiral (angle = pi / 3)
spreadfan = "f l l [f r r f l l f r r] f l l" :: Pattern IN.Turtle
starmesh = "f <l r> f <r l r>" :: Pattern IN.Turtle
bentTriangles = "f <l r> f <r l r> f" :: Pattern IN.Turtle
spiderLogo = "f [<l r> f ] <r l r> f" :: Pattern IN.Turtle
sineCurve = "[f l f r] <r l> [f <r [l f r] l> f] <r l> [f l f r]" :: Pattern IN.Turtle

--factor is pi / 7
circleMandala = "f l l f r r f l l" :: Pattern IN.Turtle
helixCircle = "f [<l r> f ] <r l r> f" :: Pattern IN.Turtle

