{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}
module ParallelPatterns where
import           DrawTurtle
import           DrawPattern
import Prelude hiding (Left, Right, (>/>))
import Graphics.WorldTurtle 
import qualified Graphics.Gloss.Data.Picture as G
import qualified Sound.Tidal.Context as Tidal
import qualified IntermediateNotation as IN
import           GetTidalTime
import           ParseTurtle

stackTurtles :: [Turtle] -> [TurtleCommand ()] -> WorldCommand ([WorldCommand ()])
stackTurtles _ [] = do return []
stackTurtles turtles commands = do
       rest <- stackTurtles (tail turtles) (tail commands)
       return (((head turtles) >/> (head commands)): rest)

sequenceMap :: [[TurtleCommand ()]] -> [TurtleCommand ()]
sequenceMap = map sequence_

stackCommands :: [WorldCommand ()] -> WorldCommand ()
stackCommands result = foldl1 (>!>) result

{-
main :: IO ()
main = WT.runWorld' WT.black $ do
           WT.clear

turtleCommands = [[right 10, forward 10], [left 100]]
-}

turtlesList2Commands :: [[IN.Turtle]] -> [[TurtleCommand()]]
turtlesList2Commands = map mapTurtles

turtles :: [[IN.Turtle]]
turtles = [[IN.Right 10, IN.Forward 10], [IN.Forward 20, IN.Right 10]]


patternList :: [Tidal.Pattern IN.Turtle] 
--patternList = ["l f" :: Tidal.Pattern IN.Turtle, Tidal.slow "1 2" tripat]
patternList = [Tidal.fast "2 2 2" tripat, Tidal.slow "1 2" tripat]

turtleCommands = sequenceMap $ turtlesList2Commands $ map (pattern2Turtles) patternList

main :: IO ()
main = runWorld' black $ do
           t1 <- makeTurtle' (82.5, 55) north yellow
           t2 <- makeTurtle' (100, 0) north red
           result <- stackTurtles [t1, t2] turtleCommands
           --bresult <- stackTurtles [t1, t2] $ sequenceMap turtleCommands
           stackCommands result
           clear
