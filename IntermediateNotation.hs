module IntermediateNotation where
import Prelude hiding (Left, Right)

data Turtle =   Left    Float
              | Right   Float
              | Forward Float 
              deriving (Show)

data Shape  =   Square    Float
              | Triangle  Float
              | Circle    Float
              deriving (Show)

data MetaShape = Spiral { 
                     shape    :: Shape
                   , shifts   :: [Turtle]
                   , delta    :: Float 
                 }
              deriving (Show)


turtleLength :: Turtle -> Float
turtleLength turtle = case turtle of
    Left a    -> a
    Right a   -> a
    Forward a -> a
